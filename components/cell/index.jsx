import Icon from '../icon'
export default {
  name: 'Cell',
  props: {
    label: String,
    text: [String,Number],
    arrow: { type: Boolean, default: true },
    placeholder: String
  },
  methods: {
    handel(e) {
      this.$emit('click', e)
    }
  },
  render() {
    let { label, $slots, text, placeholder } = this
    const labelNode = label ? <label for="" class="k-cell-label">{this.label}</label> : null
    let textNode = $slots.default ||
      ((text && text !== null && text !== undefined) && <span class="k-cell-text">{text}</span>) ||
      (placeholder && <span class="k-cell-placeholder">{placeholder}</span>)
    let arrowNode = this.arrow ? <span class="k-cell-arrow"><Icon type="ios-arrow-forward" /></span> : null
    return (
      <div class="k-cell" onClick={this.handel} >
        {labelNode}
        {textNode}
        {arrowNode}
      </div >
    )
  }
}