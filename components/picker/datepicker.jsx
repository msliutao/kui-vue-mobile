import Picker from './picker'
const now = new Date();
// import { deepCopy } from '../_tool/copy'

export default {
  name: 'DatePicker',
  props: {
    showTime: Boolean,
    format: String,
    start: {
      type: [String, Number],
      default: now.getFullYear() - 20
    },
    end: {
      type: [Number, String],
      default: now.getFullYear() + 20
    },
    value: {
      type: [Number, String, Date],
      // default: () => []
    },
    placeholder: {
      type: String,
      default: '请选择日期'
    }
  },
  watch: {
    value(v) {
      this.currentValue = v
    }
  },
  data() {
    return {
      currentValue: this.value
    }
  },
  methods: {
    getDate(value) {
      return new Date(value.replace(/-/g, '/'))
    },
    getValue() {
      let values = []
      let value = this.value
      if (value) {
        if (typeof value == 'string') {
          value = this.getDate(value)
        }
        value = new Date(value)
        values[0] = value.getFullYear()
        values[1] = value.getMonth() + 1
        values[2] = value.getDate()
        if (this.showTime) {
          values[3] = value.getHours()
          values[4] = value.getMinutes()
          values[5] = value.getSeconds()
        }
      }
      return values
    },
    getDays(y, m) {
      return new Date(y, m, 0).getDate() //就是每个月的1号，把它设置为0时， new Date()会返回上一个月的最后一天
    },
    getChildren(month, min, max) {
      let year = this.getValue()[0]
      if (!year) {
        year = now.getFullYear()
      }
      let count = this.getDays(year, month)
      let D = []
      max = Math.min(max, count)
      for (let i = min; i <= max; i++) {
        D.push({ label: i + '日', value: i })
      }
      return D;
    },
    getData() {
      let { start, end } = this
      let startDate, endDate;
      if (typeof start == 'number') {
        startDate = new Date(`${start}/01/01 00:00:00`);
      } else if (typeof start == 'string') {
        startDate = this.getDate(start);
      }

      if (typeof end == 'number') {
        endDate = new Date(`${end}/12/31 23:59:59`);
      } else if (typeof end == 'string') {
        endDate = this.getDate(end);
      }
      let Y = [], M = [], D = [];
      for (let y = startDate.getFullYear(); y <= endDate.getFullYear(); y++) {
        Y.push({ label: y + '年', value: y })
      };
      let startDay = startDate.getDate(), endDay = endDate.getDate();
      for (let m = startDate.getMonth() + 1; m <= endDate.getMonth() + 1; m++) {
        let children = this.getChildren(m, startDay, endDay)
        M.push({ label: m + '月', value: m, children })
      }
      let m = this.getValue()[1] || now.getMonth() + 1
      if (!M[m - 1]) {  //如果没有默认值
        D = this.getChildren(m, startDay, endDay)
      } else {
        D = M[m - 1].children
      }
      if (this.showTime) {
        let HH = [], mm = [], ss = []
        //hours
        let hStart = startDate.getHours(), hEnd = endDate.getHours()
        for (let i = hStart; i <= hEnd; i++) {
          HH.push({ label: ('0' + i).slice(-2), value: i })
        }
        //minutes
        let mStart = startDate.getMinutes(), mEnd = endDate.getMinutes()
        for (let i = mStart; i <= mEnd; i++) {
          mm.push({ label: ('0' + i).slice(-2), value: i })
        }
        //minutes
        let sStart = startDate.getSeconds(), sEnd = endDate.getSeconds()
        for (let i = sStart; i <= sEnd; i++) {
          ss.push({ label: ('0' + i).slice(-2), value: i })
        }
        return [Y, M, D, HH, mm, ss]
      }
      return [Y, M, D]
    }
  },
  render() {
    let { currentValue, placeholder, format } = this
    let value = this.getValue()
    format = format || (this.showTime ? 'YYYY-MM-DD HH:mm:ss' : 'YYYY-MM-DD')
    let data = this.getData()
    // console.log(value)
    let props = {
      props: {
        mode: this.showTime ? 'datetime' : 'date',
        value,
        format,
        cascade: true,
        data,
        placeholder
      },
      on: {
        input: e => {
          // let date = e.join('-')
          // this.currentValue = date
        },
        ok: date => {
          // console.log(date)
          this.$emit('input', date)
          this.$emit('ok')
        },
        cancel: e => this.$emit('cancel'),
        change: (item, index) => {
          if (item && item.children && this.currentValue) {
            // let value = this.currentValue
            // if (typeof value == 'string') {
            //   value = this.getDate(value)
            // }
            // value.setMonth(item.value - 1)
            // if (this.currentValue != value) {
            //   this.currentValue = value
            //   this.$emit('change', item, index)
            // }
          }
        }
      }
    }
    return (
      <Picker {...props} />
    )
  }
}