import Icon from '../icon'
export default {
  props: {
    mini: Boolean,
    value: Boolean,
    label: String
  },
  data() {
    return {
      checked: this.value
    }
  },
  watch: {
    value(v) {
      this.checked = v
    }
  },
  methods: {
    change() {
      this.checked = !this.checked
      this.$emit('input', !this.value)
      this.$emit('change', this.checked)
    }
  },
  render() {
    //<Icon type="ios-radio-button-off" />
    //<Icon type="ios-checkmark-circle" />
    const classes = ['k-checkbox', { 'k-checkbox-mini': this.mini, 'k-checkbox-checked': this.checked }]
    const icon = this.checked ? 'ios-checkmark-circle' : 'ios-radio-button-off'
    return (
      <div class={classes} onClick={this.change} >
        <div class="k-checkbox-inner">
          <span class="k-checkbox-icon"><Icon type={icon} /></span>
          {this.label ? <span class="k-checkbox-label">{this.label}</span> : null}
          {this.$slots.default}
        </div>
      </div >
    )
  }
}