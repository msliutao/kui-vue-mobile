export default {
  name: 'Views',
  render() {
    let height = (this.$route.query.statusBarHeight || 0) + 'px'
    const styles = { height }
    return (
      <div class="jd-views" >
        <div class="temp" style={styles}></div>
        {this.$slots.default}
      </div >
    )
  }
}