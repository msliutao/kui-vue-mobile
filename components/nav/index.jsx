// import Icon from '../icon'
import { hasProp } from '../_tool/utils'
export default {
  name: "Nav",
  props: {
    title: String,
    alpha: Boolean,
    white: { type: Boolean, default: true },
    navBack: Function,
    statusBarColor: String,
    hasNavBack: { type: Boolean, default: true },
    alphaTop: {
      type: Number,
      default: 20
    }
  },
  data() {
    return {
      opacity: this.alpha ? 0 : 1,
      statusBarHeight: 0,
      hideNav: 0,
      hideNavBack: false,
      oldTop: 0,
    }
  },
  mounted() {
    let { statusBarHeight = 0, hideNav = 0, hasNavBack = 0 } = this.$route.query
    this.statusBarHeight = statusBarHeight
    this.hideNav = hideNav
    this.hideNavBack = this.hasNavBack || (hasNavBack == 1)
    this.setBarColor();
    if (this.alpha) {
      this.setOpacity()
      // let target = this.getTarget()
      window.addEventListener('scroll', this.setOpacity)
      // document.body.addEventListener('touched', this.setOpacity)
    }
  },
  beforeDestroy() {
    if (this.alpha) {
      // let target = this.getTarget()
      window.removeEventListener('scroll', this.setOpacity)
      // document.body.removeEventListener('touched', this.setOpacity)
    }
  },
  methods: {
    // getTarget() {
    //   return document.body.offsetHeight == window.innerHeight ? document.body : window
    // },
    setBarColor() {
      let color = this.statusBarColor || (this.white && !this.alpha ? 'black' : 'white')
      window.setBarColor(color)
    },
    setOpacity() {
      let s = document.documentElement.scrollTop || document.body.scrollTop;
      // if (s < 0) return;
      let { alphaTop, statusBarColor } = this
      let isUP = this.oldTop < s
      if (isUP && this.opacity < 1) {
        this.opacity += 0.02
        this.opacity = Math.min(this.opacity, 1)
        if (this.opacity >= 0.5) {
          window.setBarColor(statusBarColor || 'black')
          this.$emit('change', this.opacity)
        }
      } else if (s <= alphaTop && this.opacity > 0) {
        this.opacity -= 0.02
        this.opacity = Math.max(this.opacity, 0)
        if (this.opacity <= 0.5) {
          this.$emit('change', this.opacity)
          window.setBarColor(statusBarColor || 'white')
        }
      }
      if (s <= 0) {
        this.opacity = 0
        this.$emit('change', this.opacity)
        window.setBarColor(statusBarColor || 'white')
      }
      if (s >= alphaTop && this.opacity != 1) {
        window.setBarColor(statusBarColor || 'black')
        this.opacity = 1
        this.$emit('change', this.opacity)
      }
      this.oldTop = s
    },
    back() {
      if (this.hideNavBack) {
        if (hasProp(this, 'navBack') && typeof this.navBack == 'function') {
          this.navBack()
        } else {
          if (typeof window.navBack == 'function') window.navBack()
        }
      }
    }
  },
  render() {
    let { $slots, white, opacity } = this
    let barStyle = { paddingTop: this.statusBarHeight + 'px' }
    if (this.alpha) {
      barStyle.backgroundColor = `rgba(255,255,255,${opacity})`
      // if (this.white) {
      barStyle.borderColor = `rgba(242,242,242,${opacity})`
      // }
    }
    let props = {
      class: ['nav-bar', {
        'nav-bar-white': white,
        'nav-bar-alpha': opacity <= .5,
        'nav-bar-no-nav-back': !this.hideNavBack,
        'nav-bar-dark': window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches
      }],
      style: barStyle
    }
    return (this.hideNav == 1 ? null :
      <div class="nav-bar-title">
        <div {...props}>
          <div class="nav-left" onClick={this.back}>
            <i class="k-icon"><svg width="1em" height="1em" viewBox="0 0 20 35" fill="currentColor">
              <path d="M2.56066017,14.8745113 L17.4099026,29.2737767 C17.995689,29.841812 17.995689,30.7627793 17.4099026,31.3308146 C16.8241161,31.89885 15.8743687,31.89885 15.2885822,31.3308146 L0.439339828,16.9315493 C-0.146446609,16.3635139 -0.146446609,15.4425467 0.439339828,14.8745113 C1.02512627,14.306476 1.97487373,14.306476 2.56066017,14.8745113 Z M0.439339828,14.8252919 L15.2885822,0.4260265 C15.8743687,-0.142008833 16.8241161,-0.142008833 17.4099026,0.4260265 C17.995689,0.994061834 17.995689,1.91502908 17.4099026,2.48306441 L2.56066017,16.8823298 C1.97487373,17.4503651 1.02512627,17.4503651 0.439339828,16.8823298 C-0.146446609,16.3142944 -0.146446609,15.3933272 0.439339828,14.8252919 Z" id="矩形"></path>
            </svg></i>
          </div>
          <div class="nav-title" style={{ 'opacity': opacity }}>
            <div class="nav-title-text">
              {$slots.title || this.title}
            </div>
          </div>
          <div class="nav-right" style={{ 'opacity': opacity }}>
            {$slots.right}
          </div>
        </div>
      </div >
    )
  }
}