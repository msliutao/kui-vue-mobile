export default {
  name: 'SwiperItem',
  render() {
    return <div class="k-swiper-item" onClick={this.clickHandle}>
      {this.$slots.default}
    </div>
  },
  methods: {
    clickHandle(e) {
      this.$emit('click', e)
    }
  }
}