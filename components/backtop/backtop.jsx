
import Icon from "../icon";
import scroll from "../_tool/scroll";
import { scrollToOffset } from '../_tool/utils'
export default {
  name: "BackTop",
  directives: { scroll },
  props: {
    height: { type: [String, Number], default: 100 },
    right: [String, Number],
    bottom: [String, Number],
    target: {
      type: Function,
      default: () => {
        return window;
      }
    }
  },
  data() {
    return {
      timer: null,
      visible: false
    };
  },
  computed: {
    styles() {
      return {
        bottom: `${this.bottom}px`,
        left: `${this.right}px`
      }
    }
  },
  methods: {
    scroll() {
      let top = document.body.scrollTop || document.documentElement.scrollTop || window.scrollY;
      this.visible = top >= this.height;
    },
    handle(e) {
      this.$emit("click", e);
      if (this.timer) {
        clearInterval(this.timer);
      }
      this.timer = scrollToOffset(this.target(), 0)
      // let height = 80;
      // this.timer = setInterval(() => {
      //   var oTop = document.body.scrollTop || document.documentElement.scrollTop || window.scrollY;
      //   if (oTop > 0) {
      //     document.body.scrollTop = document.documentElement.scrollTop = oTop - height;
      //     // window.setWindowScroll(-height)
      //     this.scroll()
      //   } else {
      //     clearInterval(this.timer);
      //   }
      //   if (height <= 15) height = 15;
      //   else height -= 1;
      // }, 10);
      //ie 9 不兼容 cancelAnimationFrame
      // cancelAnimationFrame(this.timer);
      // let _this = this
      // this.timer = requestAnimationFrame(function fn() {
      //     var oTop = document.body.scrollTop || document.documentElement.scrollTop;
      //     if (oTop > 0) {
      //         document.body.scrollTop = document.documentElement.scrollTop = oTop - 150;
      //         _this.timer = requestAnimationFrame(fn);
      //     } else {
      //         cancelAnimationFrame(_this.timer);
      //     }
      // });
    }
  },
  render() {
    let child = this.$slots.default
    if (!child) {
      child = <div class="k-backtop-content"><Icon type="md-arrow-round-up" /></div>
    }
    return (
      <transition name="k-backtop-fade">
        <div class="k-backtop" onClick={this.handle} v-show={this.visible} v-scroll={this.scroll} style={this.styles} >
          {child}
        </div>
      </transition>
    )
  }
}; 