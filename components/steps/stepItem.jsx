import Icon from '../icon'
export default {
  name: 'StepItem',
  props: {
    title: String,
    icon: String,
    status: String
  },
  render() {
    let { icon, status } = this

    let classes = ['k-step-item', {
      [`k-step-${status}`]: status
    }]
    if (status == 'finish') {
      icon = "ios-checkmark"
    }
    if (status == 'error') {
      icon = 'ios-close'
    }
    if (status == 'process') {
      icon = 'ios-hourglass'
    }
    let iconNode = this.$slots.icon || <Icon type={icon} />
    return (
      <div class={classes}>
        <div class="k-step-icon">{iconNode}</div>
        <div class="k-step-text">{this.title}</div>
      </div>
    )
  }
}