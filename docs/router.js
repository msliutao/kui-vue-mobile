import Vue from 'vue'
//Vue.use(kui);
import Router from 'vue-router'

Vue.use(Router)
let router = []

router.push(
    { path: '/', component: () => import(/*webpackChunkName:'home'*/'./views/index') },
    { path: '/test', component: () => import(/*webpackChunkName:'test'*/'./test') },
)

let routers = new Router({
    routes: router, mode: 'hash', scrollBehavior(to, from, savedPosition) {
        //return期望滚动到哪个的位置
        savedPosition || { x: 0, y: 0 }
    }
})
export default routers