<p align="center">
    <a href="https://vue.k-ui.cn">
        <img width="100" src="https://chuchur.com/kui/vue/logo.svg">
    </a>
</p>

# 移动端的 UI 组件库 （kui-vue-mobile）

基于 VUE 2.x 开发，漂亮的 UI  
欢迎批评、指正、吐槽、[Star](https://github.com/chuchur-china/kui-vue-mobile) 和 [捐助](https://vue.k-ui.cn/#/sponsor)

## 注意事项

UI 使用 750 比例 1:1 ，请自行比例缩放

## 开发

```xml
npm run dev
```

## 文档

Docs: 还在写...

## For React & Vue

[kui-react](https://react.k-ui.cn)  
[kui-vue](https://vue.k-ui.cn)

## 更新日志：

Logs:

### v1.0.1 ~2020-06-18

> 增加 Affix, Checkbox, CheckboxGroup, Radio, RadioGroup, RadioButton, Button, Cell, CellAction, Nav, Views, Icon, Loading, Switch, Steps, StepItem, Picker, DatePicker,Pull, TimeLine 等组件
> 移除了 Drawer 组件，它不属于这个世界^_^!
### v1.0.0 ~2019-10-29

> 当前只有 Drawer、Message、Modal、Popup、Swiper 等组件

## 安装

### 使用 npm

```xml
npm install kui-vue-mobile --save
```
