module.exports = {
  presets: [
    [
      "@babel/preset-env",
      {
        useBuiltIns: "usage",
        corejs: "3.6.4",
      },
    ],
  ],
  plugins: [
    "transform-vue-jsx",
    "transform-class-properties",
    "@babel/plugin-syntax-dynamic-import",
    "@babel/plugin-transform-arrow-functions",
  ],
};
